﻿1
00:00:00,240 --> 00:00:04,080
So, we are going to be looking at the

2
00:00:02,560 --> 00:00:05,680
_ETHREAD structure

3
00:00:04,080 --> 00:00:09,359
on Vergilus

4
00:00:05,680 --> 00:00:09,359
for Windows 10 1809.

5
00:00:14,719 --> 00:00:20,720
So, we can see that the first

6
00:00:18,240 --> 00:00:23,119
element of the _ETHREAD structure is

7
00:00:20,720 --> 00:00:25,359
a _KTHREAD and it's inlined, it's not a

8
00:00:23,119 --> 00:00:27,199
pointer, which means the _KTHREAD

9
00:00:25,359 --> 00:00:29,519
structure is

10
00:00:27,199 --> 00:00:31,439
0x5f0 in size.

11
00:00:29,519 --> 00:00:33,840
This can be confirmed by clicking on

12
00:00:31,439 --> 00:00:33,840
_KTHREAD.

13
00:00:39,040 --> 00:00:43,280
So, if we have a pointer to the _ETHREAD

14
00:00:41,280 --> 00:00:45,680
it will actually be the same as a

15
00:00:43,280 --> 00:00:49,039
pointer to the _KTHREAD.

16
00:00:45,680 --> 00:00:49,039
So, if we look at the _KTHREAD,

17
00:00:50,079 --> 00:00:56,000
we see it has a _KAPC_STATE structure

18
00:00:53,440 --> 00:00:56,000
embedded.

19
00:00:57,600 --> 00:01:01,199
And this structure holds a pointer to a

20
00:01:00,000 --> 00:01:04,719
_KPROCESS.

21
00:01:01,199 --> 00:01:04,719
So, if we look at the _KPROCESS,

22
00:01:05,920 --> 00:01:10,799
we can see it's referenced by the

23
00:01:09,040 --> 00:01:13,840
_EPROCESS.

24
00:01:10,799 --> 00:01:16,479
And if we look at the _EPROCESS,

25
00:01:13,840 --> 00:01:18,640
we see the first member of the _EPROCESS

26
00:01:16,479 --> 00:01:20,880
is actually a _KPROCESS. So, again what

27
00:01:18,640 --> 00:01:23,680
that means is that a pointer to a

28
00:01:20,880 --> 00:01:26,640
_EPROCESS is the same as a pointer to a

29
00:01:23,680 --> 00:01:26,640
_KPROCESS.

