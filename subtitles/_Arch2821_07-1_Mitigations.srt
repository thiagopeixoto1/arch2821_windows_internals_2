1
00:00:00,320 --> 00:00:03,760
So, in this part, we're going to look into

2
00:00:01,920 --> 00:00:05,520
all the mitigations that have been added

3
00:00:03,760 --> 00:00:08,400
into the Windows kernel. So, what is a

4
00:00:05,520 --> 00:00:10,080
mitigation? A mitigation is basically a way

5
00:00:08,400 --> 00:00:12,000
to reduce the likelihood that a

6
00:00:10,080 --> 00:00:15,200
vulnerability is exploited successfully.

7
00:00:12,000 --> 00:00:17,359
A mitigation can entirely close a bug

8
00:00:15,200 --> 00:00:19,600
class and make it not exploitable at all,

9
00:00:17,359 --> 00:00:21,119
or it can make it more difficult to

10
00:00:19,600 --> 00:00:22,400
exploit it. So, let's look at the

11
00:00:21,119 --> 00:00:24,320
different mitigations that have been

12
00:00:22,400 --> 00:00:27,519
added into Windows over time. Let's get

13
00:00:24,320 --> 00:00:30,240
started. So, NX is basically the idea that

14
00:00:27,519 --> 00:00:32,960
most memory that is writable won't be

15
00:00:30,240 --> 00:00:35,200
executable, it is implemented in the page

16
00:00:32,960 --> 00:00:38,320
tables that basically dictates whether

17
00:00:35,200 --> 00:00:40,160
or not you can write or execute. The idea

18
00:00:38,320 --> 00:00:42,079
is you can't just introduce shellcode

19
00:00:40,160 --> 00:00:43,760
into arbitrary locations in kernel

20
00:00:42,079 --> 00:00:45,680
memory, and then just overwrite a

21
00:00:43,760 --> 00:00:48,399
function pointer to jump there. The idea

22
00:00:45,680 --> 00:00:50,960
is you have to at least bypass

23
00:00:48,399 --> 00:00:53,199
non-executable memory protection somehow

24
00:00:50,960 --> 00:00:55,840
first, be it return oriented programming

25
00:00:53,199 --> 00:00:58,320
technique, like ROP, or manipulating page

26
00:00:55,840 --> 00:01:00,559
tables, for instance. So, NX was present in

27
00:00:58,320 --> 00:01:03,280
most places into the Windows kernel but

28
00:01:00,559 --> 00:01:04,879
not in the non-paged pool initially. So, if

29
00:01:03,280 --> 00:01:06,720
through some kernel vulnerability, you

30
00:01:04,879 --> 00:01:09,600
could leak the address of a non-paged

31
00:01:06,720 --> 00:01:11,600
pool chunk with data you control, you

32
00:01:09,600 --> 00:01:13,760
could store a shellcode there, and then

33
00:01:11,600 --> 00:01:15,600
find a way to jump into that chunk to

34
00:01:13,760 --> 00:01:17,360
execute the shellcode from there. But,

35
00:01:15,600 --> 00:01:19,759
since Windows 8, you can't do that

36
00:01:17,360 --> 00:01:21,840
anymore, because NX is also enabled for

37
00:01:19,759 --> 00:01:23,439
the non-paged pool. Most things in the

38
00:01:21,840 --> 00:01:25,920
Windows kernel memory space are

39
00:01:23,439 --> 00:01:28,880
randomized now, since Windows vista, so

40
00:01:25,920 --> 00:01:31,360
you're almost always looking for memory

41
00:01:28,880 --> 00:01:33,759
disclosure vulnerabilities, or figuring

42
00:01:31,360 --> 00:01:36,159
out ways to create them. A lot of the

43
00:01:33,759 --> 00:01:39,119
time, for local privilege escalations,

44
00:01:36,159 --> 00:01:41,520
unless you are in a sandbox, and you have

45
00:01:39,119 --> 00:01:43,920
limited access to system calls, you can

46
00:01:41,520 --> 00:01:46,560
almost always leak kernel addresses,

47
00:01:43,920 --> 00:01:48,960
because the default state that a process

48
00:01:46,560 --> 00:01:50,880
runs in, you can leak all sorts of stuff.

49
00:01:48,960 --> 00:01:53,360
It is only when you are in a low

50
00:01:50,880 --> 00:01:56,399
integrity level, typically in a sandbox,

51
00:01:53,360 --> 00:01:59,840
that the win32k syscall filtering is

52
00:01:56,399 --> 00:02:03,200
in place, that your capability to leak

53
00:01:59,840 --> 00:02:06,320
kernel addresses is significantly reduced. In

54
00:02:03,200 --> 00:02:10,000
general, until recently at least, kernel

55
00:02:06,320 --> 00:02:12,640
ASLR has been mostly in place to prevent

56
00:02:10,000 --> 00:02:15,440
remote exploitation, but it hasn't been

57
00:02:12,640 --> 00:02:18,319
used for real local privilege escalation

58
00:02:15,440 --> 00:02:19,599
mitigation. The main exceptions to

59
00:02:18,319 --> 00:02:22,160
everything being randomized in the

60
00:02:19,599 --> 00:02:24,239
Windows kernel were the HAL, like the

61
00:02:22,160 --> 00:02:27,120
Hardware Abstraction Layer, that's been

62
00:02:24,239 --> 00:02:29,360
randomized in Windows 10 1703.

63
00:02:27,120 --> 00:02:31,440
Ans similarly the page tables themselves

64
00:02:29,360 --> 00:02:33,360
were not randomized either, in the past.

65
00:02:31,440 --> 00:02:35,360
And there was a trick, if you had an

66
00:02:33,360 --> 00:02:37,680
arbitrary write primitive, you could just

67
00:02:35,360 --> 00:02:39,200
modify the page tables themselves by

68
00:02:37,680 --> 00:02:41,519
knowing exactly where they are, and

69
00:02:39,200 --> 00:02:44,319
bypass things like SMEP. But that has

70
00:02:41,519 --> 00:02:46,800
also been fixed in the recent Windows 10 1703.

71
00:02:44,319 --> 00:02:48,959
In the past, you could allocate the

72
00:02:46,800 --> 00:02:52,000
NULL page and that is now prevented by

73
00:02:48,959 --> 00:02:54,000
allocating it when a new process starts.

74
00:02:52,000 --> 00:02:56,080
So you can't do it anymore, and that has

75
00:02:54,000 --> 00:02:58,480
been present since Windows 8. This

76
00:02:56,080 --> 00:03:01,680
basically killed the entire NULL

77
00:02:58,480 --> 00:03:04,480
dereference bug class. SMEP stands for

78
00:03:01,680 --> 00:03:06,959
Supervisor Mode Execution Protection and

79
00:03:04,480 --> 00:03:09,519
the idea is that it prevents kernel mode

80
00:03:06,959 --> 00:03:11,920
to execute userland pages and it is

81
00:03:09,519 --> 00:03:14,319
enforced through the page tables. The

82
00:03:11,920 --> 00:03:16,239
idea is that if you get code execution

83
00:03:14,319 --> 00:03:19,200
into the kernel somehow, you won't be

84
00:03:16,239 --> 00:03:21,519
able to redirect execution to your own

85
00:03:19,200 --> 00:03:24,000
userland allocations. This has been

86
00:03:21,519 --> 00:03:26,319
present since Windows 8. We mentioned

87
00:03:24,000 --> 00:03:28,080
the Object Manager and that every object

88
00:03:26,319 --> 00:03:30,959
that is tracked by the Object Manager

89
00:03:28,080 --> 00:03:33,200
has its own OBJECT_HEADER, which is

90
00:03:30,959 --> 00:03:35,440
prefixed in front of the structure

91
00:03:33,200 --> 00:03:37,599
specific to the object type, like an

92
00:03:35,440 --> 00:03:40,239
_ETHREAD or _EPROCESS. And one of the

93
00:03:37,599 --> 00:03:42,799
fields of the OBJECT_HEADER is the Type[Index]

94
00:03:40,239 --> 00:03:45,040
which basically dictates the type of the

95
00:03:42,799 --> 00:03:47,840
object. And so there is a global table in

96
00:03:45,040 --> 00:03:50,799
the kernel that is indexed based on this

97
00:03:47,840 --> 00:03:53,120
TypeIndex. And it used to be that, if you

98
00:03:50,799 --> 00:03:56,000
could overwrite the Type[Index] field of a given

99
00:03:53,120 --> 00:03:57,920
OBJECT_HEADER in one of the pools, then

100
00:03:56,000 --> 00:04:00,080
when it did the lookup, you would end up

101
00:03:57,920 --> 00:04:02,239
being able to force the kernel to access

102
00:04:00,080 --> 00:04:03,840
userland memory and then execute a

103
00:04:02,239 --> 00:04:05,920
function pointer that is controlled in

104
00:04:03,840 --> 00:04:08,159
userland. So, they introduced field

105
00:04:05,920 --> 00:04:09,920
encoding. So, now you need to be able to

106
00:04:08,159 --> 00:04:11,439
leak the cookie in order to know the

107
00:04:09,920 --> 00:04:14,239
layout of the object, in order to

108
00:04:11,439 --> 00:04:15,920
manipulate its data, if you want to still

109
00:04:14,239 --> 00:04:17,840
use that technique, which means, in

110
00:04:15,920 --> 00:04:19,600
practice, this technique can't be used

111
00:04:17,840 --> 00:04:21,519
anymore. Here we just link a couple of

112
00:04:19,600 --> 00:04:23,919
papers if you want more information on

113
00:04:21,519 --> 00:04:26,000
this. Something else that used to be

114
00:04:23,919 --> 00:04:28,960
really common exploitation technique was

115
00:04:26,000 --> 00:04:31,759
to abuse unlinking operations in

116
00:04:28,960 --> 00:04:34,240
doubly-linked list in the kernel. So, in

117
00:04:31,759 --> 00:04:36,320
the old days when you were doing heap

118
00:04:34,240 --> 00:04:38,240
exploitation stuff, if you could control

119
00:04:36,320 --> 00:04:40,800
the forward and backward pointers, you

120
00:04:38,240 --> 00:04:43,120
could often build a write primitive and

121
00:04:40,800 --> 00:04:45,280
effectively write some value you control

122
00:04:43,120 --> 00:04:47,120
to some location and that was really

123
00:04:45,280 --> 00:04:49,440
really common across lots of software

124
00:04:47,120 --> 00:04:51,680
and operation systems. The exploitation

125
00:04:49,440 --> 00:04:54,160
technique relies on the fact that when

126
00:04:51,680 --> 00:04:56,160
an element is removed from a linked list,

127
00:04:54,160 --> 00:04:58,960
it accesses the forward and backward

128
00:04:56,160 --> 00:05:01,360
pointers, and the removal of the element

129
00:04:58,960 --> 00:05:03,600
triggers two write operations. So, in the

130
00:05:01,360 --> 00:05:07,039
first write operation, it writes some

131
00:05:03,600 --> 00:05:07,039
data in a location

132
00:05:08,240 --> 00:05:12,160
and in the second write operation, it writes

133
00:05:10,320 --> 00:05:16,800
the previously used location in the

134
00:05:12,160 --> 00:05:16,800
previously used data, if that makes sense.

135
00:05:23,440 --> 00:05:27,680
What this basically means is that

136
00:05:25,600 --> 00:05:29,600
there are some constraints in the write

137
00:05:27,680 --> 00:05:32,800
primitive where you can't just write

138
00:05:29,600 --> 00:05:34,880
anything you want anywhere in memory, and

139
00:05:32,800 --> 00:05:37,199
that the value you want to write needs

140
00:05:34,880 --> 00:05:39,039
to be a valid address in the first place,

141
00:05:37,199 --> 00:05:42,240
so when the other write happens it

142
00:05:39,039 --> 00:05:44,400
doesn't crash. And so that's why usually

143
00:05:42,240 --> 00:05:46,400
we like to call this kind of primitive,

144
00:05:44,400 --> 00:05:48,320
like a mirror write primitive, because

145
00:05:46,400 --> 00:05:50,720
there are two writes in mirror, even

146
00:05:48,320 --> 00:05:53,360
though lots of people don't actually use

147
00:05:50,720 --> 00:05:55,680
that term. So this mirror write primitive

148
00:05:53,360 --> 00:05:58,400
has been mitigated now by using what we

149
00:05:55,680 --> 00:06:00,800
call safe unlinking, which just makes

150
00:05:58,400 --> 00:06:03,280
sure that, for a given entry being

151
00:06:00,800 --> 00:06:06,560
unlinked from the list, that both of its

152
00:06:03,280 --> 00:06:08,720
pointers point back to the entry through

153
00:06:06,560 --> 00:06:11,680
their adjacent entries, and their

154
00:06:08,720 --> 00:06:13,759
pointers. So, if that is not the case,

155
00:06:11,680 --> 00:06:15,919
it won't unlink the entry and just

156
00:06:13,759 --> 00:06:18,080
trigger a BSOD, so you won't be able to

157
00:06:15,919 --> 00:06:20,960
abuse the unlink operation. And so this

158
00:06:18,080 --> 00:06:23,759
was introduced in the linked list of the

159
00:06:20,960 --> 00:06:27,199
pool allocator in Windows 7, and then on

160
00:06:23,759 --> 00:06:29,440
on Windows 8, it was added in like every

161
00:06:27,199 --> 00:06:31,840
linked list everywhere into the Windows

162
00:06:29,440 --> 00:06:34,639
kernel. Because most linked list

163
00:06:31,840 --> 00:06:36,960
functionality is actually using a

164
00:06:34,639 --> 00:06:38,080
standard API in the Windows kernel, so

165
00:06:36,960 --> 00:06:39,360
they were able

166
00:06:38,080 --> 00:06:41,039
to basically

167
00:06:39,360 --> 00:06:43,360
kill the primitive across the entire

168
00:06:41,039 --> 00:06:45,520
kernel. So, what was introduced recently

169
00:06:43,360 --> 00:06:48,479
in Windows 10 to prevent speculative

170
00:06:45,520 --> 00:06:50,960
execution kernel bugs is that Kernel

171
00:06:48,479 --> 00:06:52,479
Virtual Address shadow mitigation,

172
00:06:50,960 --> 00:06:55,440
shortened to

173
00:06:52,479 --> 00:06:57,360
KVA shadow. So the basic idea is that

174
00:06:55,440 --> 00:06:59,599
there are different page tables for user

175
00:06:57,360 --> 00:07:02,720
mode and kernel mode. And so when you do a

176
00:06:59,599 --> 00:07:04,560
syscall like NtOpenFile on that diagram,

177
00:07:02,720 --> 00:07:06,720
you'll be running in user mode and so

178
00:07:04,560 --> 00:07:08,720
you'll be trapping into kernel mode and

179
00:07:06,720 --> 00:07:11,680
so your process would have a limited set

180
00:07:08,720 --> 00:07:13,599
of page tables related to your user mode,

181
00:07:11,680 --> 00:07:15,759
so you won't be able to

182
00:07:13,599 --> 00:07:18,400
do any side channel attacks through

183
00:07:15,759 --> 00:07:21,599
speculative execution. And then, in the

184
00:07:18,400 --> 00:07:23,759
kernel, it will swap to using the kernel

185
00:07:21,599 --> 00:07:25,840
mode page tables, so it can do kernel

186
00:07:23,759 --> 00:07:28,319
stuff and finally before it returns to

187
00:07:25,840 --> 00:07:29,840
user mode it will restore the user mode

188
00:07:28,319 --> 00:07:32,000
page table, so you won't be able to

189
00:07:29,840 --> 00:07:34,400
access the kernel mode page tables

190
00:07:32,000 --> 00:07:36,560
before returning to user mode.

191
00:07:34,400 --> 00:07:39,759
So, SMAP is actually a mitigation that is

192
00:07:36,560 --> 00:07:41,440
not in Windows yet, but that will really

193
00:07:39,759 --> 00:07:43,440
make it a lot harder to exploit

194
00:07:41,440 --> 00:07:46,160
vulnerabilities in general, if it ends up

195
00:07:43,440 --> 00:07:48,720
being added. And so the SMAP mitigation

196
00:07:46,160 --> 00:07:50,960
has been on other operating system, like

197
00:07:48,720 --> 00:07:54,560
Linux or Android, for some time now, and

198
00:07:50,960 --> 00:07:57,199
they call it PAN on ARM/Android. The idea

199
00:07:54,560 --> 00:08:00,240
is very similar to SMEP with like an "E"

200
00:07:57,199 --> 00:08:02,960
instead of the "A" in SMAP. But, for SMAP,

201
00:08:00,240 --> 00:08:06,000
basically, you're trying to

202
00:08:02,960 --> 00:08:08,720
prevent kernel mode to read or write any

203
00:08:06,000 --> 00:08:10,720
userland pages, whereas with SMEP, you

204
00:08:08,720 --> 00:08:13,280
are trying to prevent kernel mode to

205
00:08:10,720 --> 00:08:15,280
execute userland pages. And so the fact

206
00:08:13,280 --> 00:08:17,440
that there is no SMAP in Windows at the

207
00:08:15,280 --> 00:08:20,400
moment means we can basically trick the

208
00:08:17,440 --> 00:08:22,479
kernel code to read or write pointers or

209
00:08:20,400 --> 00:08:24,879
structures that are not effectively in

210
00:08:22,479 --> 00:08:27,039
kernel but that are in userland.

211
00:08:24,879 --> 00:08:29,840
And the way we do that is we craft the

212
00:08:27,039 --> 00:08:32,320
structures in userland, and then

213
00:08:29,840 --> 00:08:35,279
using memory corruption variabilities, we

214
00:08:32,320 --> 00:08:38,000
corrupt some pointers to point them to

215
00:08:35,279 --> 00:08:41,360
our fake userland structures. And then

216
00:08:38,000 --> 00:08:43,839
basically, the kernel will access our

217
00:08:41,360 --> 00:08:45,920
userland structures, and potentially we

218
00:08:43,839 --> 00:08:49,680
can do bad things, like we can make the

219
00:08:45,920 --> 00:08:53,040
kernel access our structures in order to

220
00:08:49,680 --> 00:08:55,040
do bad stuff. If Windows had SMAP enabled,

221
00:08:53,040 --> 00:08:57,440
basically any of these accesses to

222
00:08:55,040 --> 00:08:59,519
userland would fail. And so the way you

223
00:08:57,440 --> 00:09:02,080
would end up having to exploit the

224
00:08:59,519 --> 00:09:04,720
vulnerabilities would be introduce all

225
00:09:02,080 --> 00:09:07,839
of the data into the kernel somehow, and

226
00:09:04,720 --> 00:09:10,560
figure out where the data you introduced

227
00:09:07,839 --> 00:09:13,519
exists in kernel memory, and then finally

228
00:09:10,560 --> 00:09:15,680
point to that kernel region instead of

229
00:09:13,519 --> 00:09:18,320
using the userland structures, and so

230
00:09:15,680 --> 00:09:20,480
that is significantly more difficult.

231
00:09:18,320 --> 00:09:24,080
Microsoft did partially add support for

232
00:09:20,480 --> 00:09:26,320
SMAP for certain code path, like in 1903

233
00:09:24,080 --> 00:09:28,000
for instance, but a lot of codes still

234
00:09:26,320 --> 00:09:30,880
don't have it enabled. So, there was a

235
00:09:28,000 --> 00:09:34,279
trick to disable SMEP based on the fact

236
00:09:30,880 --> 00:09:37,440
that page tables were always at a

237
00:09:34,279 --> 00:09:40,560
non-randomized address and on the fact

238
00:09:37,440 --> 00:09:43,760
that a static index was used for page

239
00:09:40,560 --> 00:09:45,680
table self-reference. And basically

240
00:09:43,760 --> 00:09:48,080
the idea was that there used to be this

241
00:09:45,680 --> 00:09:51,200
static index into the page table

242
00:09:48,080 --> 00:09:53,600
themselves, that would reference an entry

243
00:09:51,200 --> 00:09:56,240
that allowed you to reference the page

244
00:09:53,600 --> 00:09:58,560
tables themselves. And so basically, you

245
00:09:56,240 --> 00:10:00,720
would have a static virtual address in

246
00:09:58,560 --> 00:10:03,040
the kernel that points to

247
00:10:00,720 --> 00:10:05,760
the page table itself and that would let

248
00:10:03,040 --> 00:10:07,600
you modify the page table through an

249
00:10:05,760 --> 00:10:09,920
arbitrary write primitive. And so, you

250
00:10:07,600 --> 00:10:12,720
could disable SMEP in the page table. And

251
00:10:09,920 --> 00:10:15,279
so now, basically the index of this self

252
00:10:12,720 --> 00:10:18,800
referenced entry is randomized at boot.

253
00:10:15,279 --> 00:10:18,800
So, you can't use that trick anymore.

