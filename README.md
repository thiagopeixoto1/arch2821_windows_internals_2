# Slides and materials for students for the "Architecture 2821 - Windows OS Internals 2" course

* `slides/`
    * `XX-whatever.pdf`: slides deck for a given video
    * `arch2821-windows-internals-2.pdf`: full slides deck (merging all `XX-whatever.pdf`)
* `tools/`
    * `CVE-2021-31955/`: Source and compiled executable for information disclosure vulnerability (CVE-2021-31955)
    * `TestSMEP/` : Source and compiled executable to help testing the SMEP mitigation
* `subtitles/`: fixed subtitles for the course's videos